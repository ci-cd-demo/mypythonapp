pipeline {
    agent none

    // tools {
        // Install Tools, nothing yet
    // }

    stages {

        stage('SCM') {
            agent {
                docker {
                    image 'ci-cd-demo/mypythonapp:latest'
                    registryUrl 'https://registry.gitlab.com/'
                    registryCredentialsId 'unp'
                    alwaysPull true
                    args '--add-host="sonarqube.cwxlab.fr:192.168.100.20"'
                }
            }
            steps {
                 // Get the source code from our GitLab repo
                git credentialsId: 'unp', url: 'https://gitlab.com/ci-cd-demo/mypythonapp.git'
            }
        }

        stage('Build') {
            agent {
                docker {
                    image 'ci-cd-demo/mypythonapp:latest'
                    registryUrl 'https://registry.gitlab.com/'
                    registryCredentialsId 'unp'
                    alwaysPull true
                    args '--add-host="sonarqube.cwxlab.fr:192.168.100.20"'
                }
            }
            steps { 
                    
                sh '''
                    virtualenv -p python3 venv
                    . venv/bin/activate

                    pip install -r requirements.txt
                    python -m pytest
                    coverage run -m pytest
                    mkdir -p coverage-reports
                    coverage xml -o coverage-reports/my-coverage-report.xml
                '''
            }
        }

        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'ci-cd-demo/mypythonapp:latest'
                    registryUrl 'https://registry.gitlab.com/'
                    registryCredentialsId 'unp'
                    alwaysPull true
                    args '--add-host="sonarqube.cwxlab.fr:192.168.100.20"'
                }
            }
            environment {
                SCANNER_HOME = tool 'sonar-scanner-docker'
                PROJECT_KEY = "myPythonApp"
                PROJECT_SOURCES="flaskr"
                QUALITYGATE_URL="https://jenkins.cwxlab.fr/sonarqube-webhook/"
            }

            steps {
                    withSonarQubeEnv('Sonar') { // If you have configured more than one global server connection, you can specify its name
                        sh '''$SCANNER_HOME/bin/sonar-scanner \
                        -Dsonar.projectKey=$PROJECT_KEY \
                        -Dsonar.sources=$PROJECT_SOURCES \
                        -Dsonar.webhooks.project=$QUALITYGATE_URL'''
                    }
            }
        }

        stage("Quality Gate") {
            agent {
                docker {
                    image 'ci-cd-demo/mypythonapp:latest'
                    registryUrl 'https://registry.gitlab.com/'
                    registryCredentialsId 'unp'
                    alwaysPull true
                    args '--add-host="sonarqube.cwxlab.fr:192.168.100.20"'
                }
            }
            steps {
                sleep(10)
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage("Docker") {
            agent {
                docker {
                    image 'docker:dind'
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'unp', passwordVariable: 'DOCKER_REGISTRY_PWD', usernameVariable: 'DOCKER_REGISTRY_USER')]) {
                sh '''
                    echo "$DOCKER_REGISTRY_PWD" | docker login --username=$DOCKER_REGISTRY_USER --password-stdin registry.gitlab.com
                    docker build -t ci-cd-demo/mypythonapp:$BUILD_NUMBER .
                    docker push
                '''
                }
            }
        }
    }

    post {
        success {
            echo 'Hello World'
        }
    }
}
