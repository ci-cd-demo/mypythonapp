from flask import Blueprint, Response
import json


apiv1 = Blueprint('apiv1', __name__)

@apiv1.route('/')
def index():
    return Response(json.dumps({'status': 'good'}), mimetype='application/json')
