import os

from flask import Flask

int = 'truc'  # Volontaire, temporaire

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        # DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/')
    def hello():
        return '''<html>
    <body>
        <ul>
            <li>
                <a href="/hmi">Human Machine Interface</a>
            </li>
            <li>
                <a href="/apiv1">Machine to Machine Interface v1</a>
            </li>
        </ul>
    </body>
</html>'''

    from flaskr.blueprints.hmi import hmi
    app.register_blueprint(hmi, url_prefix='/hmi')

    from flaskr.blueprints.apiv1 import apiv1
    app.register_blueprint(apiv1, url_prefix='/apiv1')

    return app
