from flaskr import create_app


def test_hmi(client):
    response = client.get('/hmi/')
    assert response.data == b'<html><body><h1>Hello World!</h1></body></html>'
    assert response.status == '200 OK'
