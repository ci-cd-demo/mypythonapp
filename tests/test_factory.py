from flaskr import create_app


def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_apiv1(client):
    response = client.get('/apiv1/')
    assert response.data == b'{"status": "good"}'
    assert response.status == '200 OK'
