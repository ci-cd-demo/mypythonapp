from flaskr import create_app


def test_index(client):
    response = client.get('/')
    assert response.status == '200 OK'
