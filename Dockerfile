FROM ubuntu:20.04


RUN \
	apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y \
		python3 \
		python3-pip

RUN mkdir -p /opt/mypythonapp
WORKDIR /opt/mypythonapp

COPY requirements.txt /opt/mypythonapp/requirements.txt
RUN pip3 install -r requirements.txt
COPY flaskr /opt/mypythonapp/flaskr/
COPY uwsgi.yaml /opt/mypythonapp/

CMD ["uwsgi", "--yml", "uwsgi.yaml"]
