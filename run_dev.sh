#!/usr/bin/env bash

export FLASK_APP=flaskr
export FLASK_ENV=development
source venv/bin/activate
flask run
